/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.measuring;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import knapsack.InstanceParser;
import knapsack.KnapsackFile;
import knapsack.KnapsackInstance;
import knapsack.Stopwatch;
import knapsack.solving.Solver;

/**
 *
 * @author etruska
 */
public class ErrorPrinter {

    private ArrayList<Solver> apx;

    private Solver optimalSolver;

    private Stopwatch watch;

    private String outputFile;

    private PrintWriter writer;

    public ErrorPrinter(Solver optimalSolver) {
        this.optimalSolver = optimalSolver;
        this.apx = new ArrayList<>();
        this.watch = new Stopwatch();

    }

    public void addApxSolver(Solver apxSolver) {
        this.apx.add(apxSolver);
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
        try {
            writer = new PrintWriter(outputFile);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ErrorPrinter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void println(String line) {
        writer.println(line);
    }

    public void calcError(String fileName) throws IOException {
        calcError(fileName, false);
    }

    public void calcError(String fileName, boolean summaries) throws IOException {

        InstanceParser parser = new InstanceParser();
        KnapsackFile file = parser.parse(fileName);
        double[] maxError = new double[apx.size()];
        double[] errorSum = new double[apx.size()];
        double[] timeSum = new double[apx.size()];

        for (KnapsackInstance i : file.getInstances()) {

            writer.print("#" + i.getId() + ";;");

            watch.start();
            optimalSolver.setKnapsackInstance(i);
            optimalSolver.setPrintSolution(false);
            optimalSolver.solve();
            writer.print(watch.getElapsedTime() + ";");

            int opt = optimalSolver.getSolutionPrice();

            writer.print(opt);

            int j = 0;
            for (Solver apxSolver : apx) {
                writer.print("; ;");
                watch.start();
                apxSolver.setKnapsackInstance(i);
                apxSolver.setPrintSolution(false);
                apxSolver.setOptimalPrice(opt);
                apxSolver.solve();
                writer.print(watch.getElapsedTime() + ";");

                int apx = (apxSolver.getBestSolution() != null) ? apxSolver.getSolutionPrice() : 0;
                writer.print(apx + ";");

                double relError = Math.abs((double) (opt - apx) / opt);
                writer.print(relError);
                if (relError > maxError[j]) {
                    maxError[j] = relError;
                }
                errorSum[j] += relError;
                timeSum[j] += watch.getElapsedTime();
                j++;
            }
            writer.println();
        }

        if (summaries) {
            createSummaryFiles(fileName, errorSum, timeSum, maxError, file.getInstances().size());
        }
        writer.close();
    }

    private void createSummaryFiles(String inputFile, double[] errorSum, double[] timeSum, double[] maxError, int instancesCount) throws FileNotFoundException {
        String name = new File(inputFile).getName();
        String timeFile = "output/4/time_" + name + ".csv";
        String errorFile = "output/4/error_" + name + ".csv";
        NumberFormat formatter = new DecimalFormat("#0.00000");

        PrintWriter timeWriter = new PrintWriter(timeFile);
        PrintWriter errorWriter = new PrintWriter(errorFile);

        String timeHeader = "#ID;", errorHeader = "#ID;", timeContent = "1;", errorContent = "1;";

        for (int k = 0; k < apx.size(); k++) {

            timeHeader += apx.get(k).getName() + " TIME;" ;
            errorHeader += apx.get(k).getName() + " MEAN ERROR;";
            errorHeader += apx.get(k).getName() + " MAX ERROR;";

            timeContent += formatter.format(timeSum[k] / instancesCount) + ";";
            errorContent += formatter.format(errorSum[k] / instancesCount) + ";";
            errorContent += formatter.format(maxError[k]) + ";";
            System.out.println(k + " Mean error: " + formatter.format(errorSum[k] / instancesCount));
            System.out.println(k + " Mean time: " + formatter.format(timeSum[k] / instancesCount));
            System.out.println(k + " Max mean error: " + formatter.format(maxError[k]));
        }
        
        timeWriter.println(timeHeader);
        timeWriter.println(timeContent);
        errorWriter.println(errorHeader);
        errorWriter.println(errorContent);
        timeWriter.close();
        errorWriter.close();
    }

}
