/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.solving;

import knapsack.KnapsackInstance;
import knapsack.Item;

/**
 *
 * @author etruska
 */
public class FPTASSolver extends Solver {

    private int neglectedBits = 1;
    private double error = 0;

    public FPTASSolver(double error) {
        this.error = error;
    }

    public void solve() {

        int size = knapsack.size();
        int maxPrice = knapsack.getMaxPrice();
        double ratio = (double) maxPrice / (double) size;
        double result = error * ratio;
        double log = log2(result);
        this.neglectedBits = Math.max((int) Math.floor(log), 0);

        KnapsackInstance appr = approximateKnapsack(knapsack);
        
        DynamicSolver solver = new DynamicSolver();
        solver.setKnapsackInstance(appr);
        solver.setPrintSolution(false);
        solver.solve();
        this.copySolution(solver.getBestSolution());

        if (printSolution) {
            printSolution();
        }
    }

    private KnapsackInstance approximateKnapsack(KnapsackInstance original) {
        KnapsackInstance newSack = new KnapsackInstance(original);

        for (Item i : newSack.getItems()) {
            //int apprPrice = Math.max(i.getPrice() >> neglectedBits, 1);
            int apprPrice = i.getPrice() >> neglectedBits;
            i.setPrice(apprPrice);
        }

        return newSack;
    }

    private void copySolution(KnapsackInstance original) {
        for (int i = 0; i < original.size(); i++) {
            if (original.getItems().get(i).isInSack()) {
                knapsack.getItems().get(i).setInSack(true);
            } else {
                knapsack.getItems().get(i).setInSack(false);
            }
            this.bestSolution = new KnapsackInstance(knapsack);
        }

    }

    public void setNeglectedBits(int neglectedBits) {
        this.neglectedBits = neglectedBits;
    }

    private double log2(double a) {
        return Math.log(a) / Math.log(2);
    }

}
