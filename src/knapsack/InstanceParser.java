/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author etruska
 */
public class InstanceParser {

    private Scanner scanner;

    public KnapsackFile parse(String fileName) throws IOException {
        //read file line by line
//        System.out.println("Parsing file: " + fileName);
        Path path = Paths.get(fileName);
        scanner = new Scanner(path);
        ArrayList<KnapsackInstance> instances = new ArrayList<KnapsackInstance>();

        scanner.useDelimiter(System.getProperty("line.separator"));
        int size = 0;
        while (scanner.hasNext()) {
            String[] fields = scanner.next().split(" ");
            KnapsackInstance instance = new KnapsackInstance();
            size = Integer.parseInt(fields[1]);
            instance.setId(Integer.parseInt(fields[0]));
            instance.setMaxWeight(Integer.parseInt(fields[2]));

            for (int i = 3; i < fields.length; i++) {

                if (i % 2 == 0) {
                    instance.addItem(i - 3, Integer.parseInt(fields[i - 1]), Integer.parseInt(fields[i]));
                }

            }
            instances.add(instance);
        }
        scanner.close();

        KnapsackFile file = new KnapsackFile(instances, fileName);
        file.setKnapsackSize(size);
        return file;
    }

}
