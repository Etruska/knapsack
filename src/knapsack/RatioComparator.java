/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

import java.util.Comparator;

/**
 *
 * @author etruska
 */
public class RatioComparator implements Comparator<Item> {

    @Override
    public int compare(Item o1, Item o2) {
        return Double.compare(o1.getRatio(), o2.getRatio());
    }
}