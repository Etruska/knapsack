/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.solving;

import knapsack.KnapsackInstance;
import java.util.ArrayList;
import knapsack.Item;

/**
 *
 * @author etruska
 */
public class BnBSolver extends Solver {

    public void solve() {
        permute(0, 0, 0);
        if (this.printSolution) {
            printSolution();
        }
    }

    private void permute(int position, int currentWeight, int currentPrice) {

        ArrayList<Item> items = this.knapsack.getItems();
        /* last item*/
        if (position >= items.size()) {
            //knapsack.printSackState();
            //System.out.println("weight: " + currentWeight + " price: " + currentPrice);

            if (currentPrice > bestPrice && currentWeight <= maxWeight) {
                /* deep copy this knapsack state as best solution */

                bestPrice = currentPrice;
                bestSolution = new KnapsackInstance(knapsack);

                //System.out.println("Best solution (" + bestPrice + "): ");
                //bestSolution.printSackState();
            }
            return;
        }

        if (currentPrice + getBranchMaxPrice(position) <= bestPrice) {
            return;
        }

        Item item = this.knapsack.getItems().get(position);

        item.setInSack(false);
        permute(position + 1, currentWeight, currentPrice);
        item.setInSack(true);

        if (currentWeight + item.getWeight() <= maxWeight) {
            permute(position + 1, currentWeight + item.getWeight(), currentPrice + item.getPrice());
        }
    }

    private int getBranchMaxPrice(int position) {
        int sum = 0;
        for (int i = position; i < this.knapsack.getItems().size(); i++) {
            sum += this.knapsack.getItems().get(i).getPrice();
        }
        return sum;
    }

}
