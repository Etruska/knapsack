/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.measuring;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author etruska
 */
public class SummaryPrinter {

    private double instPerFile = 50.0;

    /**
     * Get the value of instPerFile
     *
     * @return the value of instPerFile
     */
    public double getInstPerFile() {
        return instPerFile;
    }

    /**
     * Set the value of instPerFile
     *
     * @param instPerFile new value of instPerFile
     */
    public void setInstPerFile(double instPerFile) {
        this.instPerFile = instPerFile;
    }

    public SummaryPrinter(double instPerFile) {
        this.instPerFile = instPerFile;
    }

    public SummaryPrinter() {
    }

    public void println(String line, String outputFile) throws IOException {
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, true)));
        out.println(line);
        out.close();
    }

    public void addTimeSummaryHW2(int id, String dataFile, String outputFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(dataFile));
        String line;
        double sum[] = new double[4];
        int lineNum = 0;
        while ((line = br.readLine()) != null) {
            if (lineNum++ != 0) {
                String cells[] = line.split(";");
                sum[0] += Double.parseDouble(cells[2]);
                sum[1] += Double.parseDouble(cells[5]);
                sum[2] += Double.parseDouble(cells[9]);
                sum[3] += Double.parseDouble(cells[13]);
            }
        }

        br.close();

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, true)));
        out.print(id);
        for (int i = 0; i < sum.length; i++) {
            out.print(";" + getRelative(sum[i]));
        }
        out.println();
        out.close();
    }

    public void addErrorSummaryHW2(int id, String dataFile, String outputFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(dataFile));
        NumberFormat formatter = new DecimalFormat("#0.00000");
        String line;
        double errorSum[] = new double[3];
        double maxError[] = new double[3];
        int lineNum = 0;
        while ((line = br.readLine()) != null) {
            if (lineNum++ != 0) {

                String cells[] = line.split(";");
                double errValues[] = new double[]{Double.parseDouble(cells[7]), Double.parseDouble(cells[11]), Double.parseDouble(cells[15])};
                for (int i = 0; i < errValues.length; i++) {
                    if (errValues[i] > maxError[i]) {
                        maxError[i] = errValues[i];
                    }
                    errorSum[i] += errValues[i];
                }
            }
        }

        br.close();

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, true)));
        out.print(id);
        for (int i = 0; i < errorSum.length; i++) {
//            out.print("; ");
            out.print(";" + formatter.format(getRelative(errorSum[i])));
            out.print(";" + formatter.format(maxError[i]));
        }
        out.println();
        out.close();
    }

    public void addErrorSummaryHW4(int id, String dataFile, String outputFile) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(dataFile));
        NumberFormat formatter = new DecimalFormat("#0.00000");
        String line;
        double errorSum[] = new double[3];
        double maxError[] = new double[3];
        int lineNum = 0;
        while ((line = br.readLine()) != null) {
            if (lineNum++ != 0) {

                String cells[] = line.split(";");
                double errValues[] = new double[]{Double.parseDouble(cells[7]), Double.parseDouble(cells[11]), Double.parseDouble(cells[15])};
                for (int i = 0; i < errValues.length; i++) {
                    if (errValues[i] > maxError[i]) {
                        maxError[i] = errValues[i];
                    }
                    errorSum[i] += errValues[i];
                }
            }
        }

        br.close();

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, true)));
        out.print(id);
        for (int i = 0; i < errorSum.length; i++) {
//            out.print("; ");
            out.print(";" + formatter.format(getRelative(errorSum[i])));
            out.print(";" + formatter.format(maxError[i]));
        }
        out.println();
        out.close();
    }

    private double getRelative(long sum) {
        return (double) sum / instPerFile;
    }

    private double getRelative(double sum) {
        return (double) sum / instPerFile;
    }
}
