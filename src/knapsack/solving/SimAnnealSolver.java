package knapsack.solving;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import knapsack.Item;
import knapsack.KnapsackInstance;

/**
 *
 * @author etruska
 */
public class SimAnnealSolver extends Solver {

    private double temperature;
    private double initTemperature = 50;
    private int startTempType = 2;
    private double finalTemperature = 1;
    private double coolingFactor = 0.90;
    private int stepCounter = 0;
    private int allStepCounter = 0;
    private int lastChangeCounter = 0;
    private int maxSteps = 350;
    private int equilibriumThreshold = 250;
    private int eqThreshType = 3;
    private boolean allowNonSolutions = false;
    private boolean randomize = true;
    private String outputFolder = "output/4";
    private boolean enableLog = false;

    public SimAnnealSolver(String name) {
        this.name = name;
    }

    public SimAnnealSolver() {
    }

    @Override
    public void solve() {
        initialSetup();
        boolean stateChanged = true;

        setDefaults();

        while (!frozen() && stateChanged) {

            stepCounter = lastChangeCounter = 0;
            stateChanged = false;
            while (!equilibrium()) {

                if (tryState()) {
                    if (knapsack.getCurrentWeight() <= maxWeight && knapsack.getCurrentPrice() > bestPrice) {
                        updateBestSolution();
                    }
                    lastChangeCounter = 0;
                    stateChanged = true;
                }

                stepCounter++;
                allStepCounter++;
                lastChangeCounter++;
                if (enableLog) {
                    writeLog();
                }
            }
            //  System.out.println("#" + stepCounter + " Price: " + price + ", temp: " + temperature + ", relError: " + relError);
            cool();
        }
        if (bestSolution != null) {
            int price = this.bestSolution.getCurrentPrice();
            double relError = Math.abs((double) (optimalPrice - price) / optimalPrice);
            System.out.println("(" + allStepCounter + " steps) Best price: " + price + ", optimal price: " + optimalPrice + ", error: " + relError);
        }
    }

    private void initialSetup() {
        knapsack = getStartingState();
        temperature = initTemperature;
        stepCounter = 0;
        allStepCounter = 0;
        bestSolution = null;
        
        if (randomize) {
            int attempts = 0;
            do{
                knapsack.randomizeKnapsack();
                attempts++;
            } while(!knapsack.isValid());
            
            System.out.println("Random knapsack after " + attempts + " attempts.");
        }
    }

    private KnapsackInstance getStartingState() {
        /* TODO: try other options */
        //for now just return empty knapsack
        return knapsack;
    }

    private void updateBestSolution() {
        bestPrice = knapsack.getCurrentPrice();
        bestSolution = new KnapsackInstance(knapsack);
    }

    private boolean tryState() {

        int randomInt = new Random().nextInt(knapsack.getItems().size());
        Item randomItem = knapsack.getItems().get(randomInt);

        if (randomItem.isInSack()) {
            double delta = randomItem.getPrice();
            double random = Math.random();
            double exponent = -delta / temperature;
            double e = Math.exp(exponent);
            //System.out.println("Delta: " + delta + ", e: " + e + ", random: " + random);
            if (random < e) {
                randomItem.setInSack(false);
                //System.out.println("Trying worse state!");
                return true;
            } else {
                //System.out.println("Not trying worse state.");
            }

        } else {
            if (allowNonSolutions || this.knapsack.getCurrentWeight() + randomItem.getWeight() <= knapsack.getMaxWeight()) {
                randomItem.setInSack(true);
                //System.out.println("Adding item " + randomItem.getPrice() + " Kč");
                return true;
            } else {
                //System.out.println("Item would exceed capacity " + (knapsack.getCurrentWeight() + randomItem.getWeight()) + "/" + knapsack.getMaxPrice());
            }
        }
        return false;
    }

    private KnapsackInstance newRandomKnapsack(KnapsackInstance orig) {
        KnapsackInstance newKnapsack = new KnapsackInstance(orig);
        /* invert random item */
        Random r = new Random();
        int random = r.nextInt(knapsack.getItems().size());
        newKnapsack.getItems().get(random).invertInSack();
        return newKnapsack;
    }

    private boolean frozen() {
        return temperature <= finalTemperature;
    }

    private void writeLog() {
        PrintWriter out = null;
        try {
            String outputFile = outputFolder + "/log_" + this.knapsack.getId() + ".csv";
            out = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, true)));
            int price = knapsack.getCurrentPrice();
            double relError = Math.abs((double) (optimalPrice - price) / optimalPrice);
            if (allStepCounter == 1) {
                out.println("#Step;Error;");
            }
            out.println(allStepCounter + ";" + relError + ";");
        } catch (IOException ex) {
            Logger.getLogger(SimAnnealSolver.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
        }

    }

    private void setDefaults() {
        double averagePrice = knapsack.getAveragePrice();

        switch (startTempType) {
            case 0:
                this.temperature = Math.sqrt(averagePrice);
                break;
            case 1:
                this.temperature = averagePrice * 0.5;
                break;
            case 2:
                this.temperature = averagePrice;
                break;
            case 3:
                this.temperature = averagePrice * Math.log(averagePrice);
                break;
            case 4:
                this.temperature = averagePrice * averagePrice;
                break;
        }

        switch (eqThreshType) {
            case 0:
                this.equilibriumThreshold = (int) Math.floor(maxSteps / 8.0);
                break;
            case 1:
                this.equilibriumThreshold = (int) Math.floor(maxSteps / 4.0);
                break;
            case 2:
                this.equilibriumThreshold = (int) Math.floor(maxSteps / 2.0);
                break;
            case 3:
                this.equilibriumThreshold = (int) Math.floor(maxSteps / 1.5);
                break;
            case 4:
                this.equilibriumThreshold = (int) Math.floor(maxSteps / 1.0);
                break;
        }

        System.out.println("Defaults: initTemp(" + startTempType + "): " + temperature + ", eqThr(" + eqThreshType
                + "): " + equilibriumThreshold + ", maxSteps: " + maxSteps + ", finalTemp: " + finalTemperature
                + ", coolFactor: " + coolingFactor + ", randomize: " + randomize);
    }

    /**
     * cool, right?
     */
    private void cool() {
        temperature *= coolingFactor;
    }

    private boolean equilibrium() {
        return (stepCounter > maxSteps || lastChangeCounter > equilibriumThreshold);
    }

    public double getTemperature() {
        return temperature;
    }

    public double getInitTemperature() {
        return initTemperature;
    }

    public void setInitTemperature(double initTemperature) {
        this.initTemperature = initTemperature;
    }

    public void setFinalTemperature(double finalTemperature) {
        this.finalTemperature = finalTemperature;
    }

    public void setMaxSteps(int maxSteps) {
        this.maxSteps = maxSteps;
    }

    public void setCoolingFactor(double coolingFactor) {
        this.coolingFactor = coolingFactor;
    }

    public void setAllowNonSolutions(boolean allowNonSolutions) {
        this.allowNonSolutions = allowNonSolutions;
    }

    public void setRandomize(boolean randomize) {
        this.randomize = randomize;
    }

    public void setEnableLog(boolean enableLog) {
        this.enableLog = enableLog;
    }

    public void setStartTempType(int startTempType) {
        this.startTempType = startTempType;
    }

    public void setEqThreshType(int eqTreshType) {
        this.eqThreshType = eqTreshType;
    }

}
