/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.solving;

import knapsack.KnapsackInstance;
import knapsack.solving.Solver;
import java.util.ArrayList;
import knapsack.Item;

/**
 *
 * @author etruska
 */
public class BruteForceSolver extends Solver {

    public void solve() {
        permute(0, 0, 0);
        printSolution();
    }

    private void permute(int position, int currentWeight, int currentPrice) {

        ArrayList<Item> items = this.knapsack.getItems();
        /* last item*/
        if (position >= items.size()) {
            //knapsack.printSackState();
            //System.out.println("weight: " + currentWeight + " price: " + currentPrice);

            if (currentPrice > bestPrice && currentWeight <= maxWeight) {
                /* deep copy this knapsack state as best solution */

                bestPrice = currentPrice;
                bestSolution = new KnapsackInstance(knapsack);

                //System.out.println("Best solution (" + bestPrice + "): ");
                //bestSolution.printSackState();
            }
            return;
        }

        Item item = this.knapsack.getItems().get(position);

        item.setInSack(false);
        permute(position + 1, currentWeight, currentPrice);
        item.setInSack(true);

       // if (currentWeight + item.getWeight() <= maxWeight) {
            permute(position + 1, currentWeight + item.getWeight(), currentPrice + item.getPrice());
       // }
    }

}
