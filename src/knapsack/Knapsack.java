/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

import java.io.File;
import knapsack.measuring.ErrorPrinter;
import knapsack.solving.BruteForceSolver;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import knapsack.measuring.FileManager;
import knapsack.measuring.SimAnnealTester;
import knapsack.measuring.SummaryPrinter;
import knapsack.solving.BnBSolver;
import knapsack.solving.DynamicSolver;
import knapsack.solving.FPTASSolver;
import knapsack.solving.SimAnnealSolver;
import knapsack.solving.SimpleRatioSolver;

/**
 *
 * @author etruska
 */
public class Knapsack {

    public static void test2(String fileName, SummaryPrinter printer) throws IOException {

        InstanceParser parser = new InstanceParser();
        BruteForceSolver bruteSolver = new BruteForceSolver();
        DynamicSolver dynamicSolver = new DynamicSolver();

        BnBSolver bnbSolver = new BnBSolver();

        ErrorPrinter ePrinter = new ErrorPrinter(dynamicSolver);

        KnapsackFile file = parser.parse(fileName);

        String outputFileName = "output/fptas_" + file.getKnapsackSize() + ".csv";
        ePrinter.setOutputFile(outputFileName);
        ePrinter.println("#ID;DYNAMIC;TIME;RES;FPTAS O.2;TIME;RES;ERR;FPTAS O.4;TIME;RES;ERR;FPTAS O.6;TIME;RES;ERR");

        ePrinter.addApxSolver(new FPTASSolver(0.2));
        ePrinter.addApxSolver(new FPTASSolver(0.4));
        ePrinter.addApxSolver(new FPTASSolver(0.6));
        ePrinter.calcError(fileName);

        printer.addTimeSummaryHW2(file.getKnapsackSize(), outputFileName, "output/fptas_summary.csv");
        printer.addErrorSummaryHW2(file.getKnapsackSize(), outputFileName, "output/fptas_error.csv");
    }

    public static void hw3runTest(String fileName, SummaryPrinter printer, String param) throws IOException {
        DynamicSolver dynamicSolver = new DynamicSolver();
        BnBSolver bnbSolver = new BnBSolver();
        SimpleRatioSolver simpleSolver = new SimpleRatioSolver();

        ErrorPrinter ePrinter = new ErrorPrinter(dynamicSolver);

        //set output file
        String name = new File(fileName).getName();
        String outputFileName = "output/3/" + param + "/" + name + ".csv";
        ePrinter.setOutputFile(outputFileName);

        ePrinter.println("#ID;DYNAMIC;TIME;RES;BnB Solver;TIME;RES;ERR;Simple;TIME;RES;ERR;");
        ePrinter.addApxSolver(bnbSolver);
        ePrinter.addApxSolver(simpleSolver);

        ePrinter.calcError(fileName);

        Matcher m = Pattern.compile("_" + param + "=([0-9\\.\\-]*)_").matcher(fileName);
        String id = "0";
        while (m.find()) {
            id = m.group(1);
        }

//        printer.addTimeSummary(Double.parseDouble(id), outputFileName, "output/3/task3_" + param + "_summary.csv");
//        printer.addErrorSummary(Double.parseDouble(id), outputFileName, "output/3/task3_" + param + "_error.csv");
    }

    public static void hw3processFolder(String param) throws IOException {
        File f1 = new File("output/3/task3_" + param + "_summary.csv");
        f1.delete();
        File f2 = new File("output/3/task3_" + param + "_error.csv");
        f2.delete();

        String dataFolder = "/Users/etruska/School/PAA/hw3data";
        SummaryPrinter printer = new SummaryPrinter();

        printer.println(param + ";DYNAMIC;BnB;SIMPLE", "output/3/task3_" + param + "_summary.csv");
        printer.println(param + ";BnB MEAN ERR;BnB MAX ERR;SIMPLE MEAN ERR;SIMPLE MAX ERR", "output/3/task3_" + param + "_error.csv");

        File folder = new File(dataFolder + "/" + param);
        for (final File fileEntry : folder.listFiles()) {
            if (!fileEntry.isDirectory() && !fileEntry.getName().startsWith(".")) {
                hw3runTest(folder.getAbsolutePath() + "/" + fileEntry.getName(), printer, param);
            }
        }
    }

    public static void hw4runTest(String fileName) throws IOException {
        System.out.println("Processing file: " + fileName);
        DynamicSolver dynamicSolver = new DynamicSolver();
        SimAnnealSolver saSolver = new SimAnnealSolver();
        ErrorPrinter ePrinter = new ErrorPrinter(dynamicSolver);

        String name = new File(fileName).getName();
        String outputFileName = "output/4/" + name + ".csv";
        ePrinter.setOutputFile(outputFileName);

        ePrinter.addApxSolver(saSolver);
        ePrinter.println("#ID;DYNAMIC;TIME;RES;SimAnneal;TIME;RES;ERR;");
        ePrinter.calcError(fileName);
    }

    public static void hw4processFolder() throws IOException {
        String dataFolder = "/Users/etruska/School/PAA/hw4data/test";

        FileManager.emptyFolder(new File("output/4"));

        SimAnnealTester tester = new SimAnnealTester(dataFolder + "/knap_n=50_N=200_m=0.5_W=65_C=125_k=1_d=0.txt");
        //hw4StartTempTest(tester);
        //hw4EqThreshTest(tester);
        //hw4MaxStepsTest(tester);
        //hw4FinalTempTest(tester);
        //hw4CoolFactorTest(tester);
        //hw4RandomTest(tester);
        hw4LogTest(tester);
    }

    public static void hw4LogTest(SimAnnealTester tester) throws IOException {
        tester.addBasicTest();
        tester.runTests();
    }
    public static void hw4RandomTest(SimAnnealTester tester) throws IOException {
        tester.addBasicTest();
        tester.addBasicRandomTest();
        tester.runTests();
    }
    public static void hw4CoolFactorTest(SimAnnealTester tester) throws IOException {
        tester.addCoolFactorTest(0.5);
        tester.addCoolFactorTest(0.6);
        tester.addCoolFactorTest(0.7);
        tester.addCoolFactorTest(0.8);
        tester.addCoolFactorTest(0.9);
        tester.addCoolFactorTest(0.95);
        tester.addCoolFactorTest(0.99);
        tester.runTests();
    }
    public static void hw4FinalTempTest(SimAnnealTester tester) throws IOException {
        tester.addFinalTempTest(0.01);
        tester.addFinalTempTest(0.1);
        tester.addFinalTempTest(0.5);
        tester.addFinalTempTest(1);
        tester.addFinalTempTest(2);
        tester.addFinalTempTest(4);
        tester.addFinalTempTest(8);
        tester.addFinalTempTest(16);
        tester.runTests();
    }
    public static void hw4MaxStepsTest(SimAnnealTester tester) throws IOException {
        tester.addMaxStepsTest(50);
        tester.addMaxStepsTest(150);
        tester.addMaxStepsTest(350);
        tester.addMaxStepsTest(550);
        tester.addMaxStepsTest(800);
        tester.runTests();
    }
    public static void hw4StartTempTest(SimAnnealTester tester) throws IOException {
        tester.addStartTempTest();
        tester.addStartTempTest(1);
        tester.addStartTempTest(2);
        tester.addStartTempTest(3);
        tester.addStartTempTest(4);
        tester.runTests();
    }
    public static void hw4EqThreshTest(SimAnnealTester tester) throws IOException {
        tester.addEqThreshTest(0);
        tester.addEqThreshTest(1);
        tester.addEqThreshTest(2);
        tester.addEqThreshTest(3);
        tester.addEqThreshTest(4);
        tester.runTests();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            hw4processFolder();
        } catch (IOException ex) {
            Logger.getLogger(Knapsack.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
