/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.solving;

import knapsack.KnapsackFile;
import knapsack.KnapsackInstance;

/**
 *
 * @author etruska
 */
public abstract class Solver {

    protected KnapsackInstance knapsack;

    protected int maxWeight;

    protected int bestPrice = 0;

    protected boolean printSolution = true;
    
    protected KnapsackInstance bestSolution = null;

    protected String name = "Solver";
    
    protected int optimalPrice;
    
    public Solver() {

    }

    public Solver(KnapsackInstance knapsack) {
        knapsack.printItems();
        this.knapsack = new KnapsackInstance(knapsack);
        this.maxWeight = knapsack.getMaxWeight();
    }

    public abstract void solve();
    
    public void solve(KnapsackFile file){

        System.out.println("Solving file " + file.getFilename());
        for(KnapsackInstance inst: file.getInstances()){
            setKnapsackInstance(inst);
            solve();
        }
    }
    
    public void solve(KnapsackInstance inst){
        setKnapsackInstance(inst);
        solve();
    }
    
    public void setKnapsackInstance(KnapsackInstance knapsack) {
        this.knapsack = new KnapsackInstance(knapsack);
        this.maxWeight = knapsack.getMaxWeight();
        this.bestPrice = 0;
        this.bestSolution = null;
    }

    protected void printSolution() {
      
        String className = this.getClass().getName().substring(this.getClass().getName().lastIndexOf(".") + 1);
        System.out.println("#" + bestSolution.getId() + " " + className +" Final solution (" + bestSolution.getCurrentWeight() + " kg, "
                + " " + bestSolution.getCurrentPrice() + " Kč): ");
        bestSolution.printSackState();
    }

    public void setPrintSolution(boolean printSolution) {
        this.printSolution = printSolution;
    }
    
    public int getSolutionPrice(){
        return bestSolution.getCurrentPrice();
    }

    public KnapsackInstance getBestSolution() {
        return bestSolution;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOptimalPrice(int optimalPrice) {
        this.optimalPrice = optimalPrice;
    }
    
}
