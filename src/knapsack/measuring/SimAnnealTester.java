/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.measuring;

import java.io.IOException;
import java.util.ArrayList;
import knapsack.solving.DynamicSolver;
import knapsack.solving.SimAnnealSolver;

/**
 *
 * @author etruska
 */
public class SimAnnealTester {

    private String inputFile;
    private String header = "#ID;DYNAMIC;TIME;RES;";
    private ArrayList<SimAnnealSolver> apxSolvers;

    public SimAnnealTester(String inputFile) {
        this.inputFile = inputFile;
        this.apxSolvers = new ArrayList<>();
    }

    public void addStartTempTest() throws IOException {
        addStartTempTest(0);
    }

    public void addStartTempTest(int tempType) throws IOException {
        header += "Basic T" + tempType + ";TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver("Basic T" + tempType);
        saSolver.setStartTempType(tempType);
        apxSolvers.add(saSolver);
    }

    public void addEqThreshTest(int threshType) throws IOException {
        header += "EQT " + threshType + ";TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver("EQT " + threshType);
        saSolver.setEqThreshType(threshType);
        apxSolvers.add(saSolver);
    }

    public void addMaxStepsTest(int steps) throws IOException {
        header += steps + ";TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver(steps + "");
        saSolver.setMaxSteps(steps);
        apxSolvers.add(saSolver);
    }
    
    public void addFinalTempTest(double temp) throws IOException {
        header += temp + ";TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver(temp + "");
        saSolver.setFinalTemperature(temp);
        apxSolvers.add(saSolver);
    }
    
    public void addCoolFactorTest(double factor) throws IOException {
        header += factor + ";TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver(factor + "");
        saSolver.setCoolingFactor(factor);
        apxSolvers.add(saSolver);
    }

    public void addBasicRandomTest() throws IOException {

        header += "BasicRandom;TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver("Basic random");
        saSolver.setRandomize(true);
        apxSolvers.add(saSolver);

    }
    public void addBasicTest() throws IOException{
        addBasicTest(false);
    }
    public void addBasicTest(boolean log) throws IOException {

        header += "Basic;TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver("Basic");
        saSolver.setRandomize(false);
        if(log){
            saSolver.setEnableLog(log);
        }
        apxSolvers.add(saSolver);
    }

    public void addNonSolutionsTest() throws IOException {

        header += "BasicRandom;TIME;RES;ERR;";
        SimAnnealSolver saSolver = new SimAnnealSolver("NonSolutions");
        saSolver.setAllowNonSolutions(true);
        apxSolvers.add(saSolver);
    }

    public void runTests() throws IOException {
        ErrorPrinter ePrinter = getEPrinter();
        ePrinter.setOutputFile("output/4/simanneal.csv");
        ePrinter.println(header);

        for (SimAnnealSolver saSolver : apxSolvers) {
            ePrinter.addApxSolver(saSolver);
        }

        ePrinter.calcError(inputFile, true);
    }

    private ErrorPrinter getEPrinter() {
        ErrorPrinter printer = new ErrorPrinter(new DynamicSolver());
        return printer;
    }
}
