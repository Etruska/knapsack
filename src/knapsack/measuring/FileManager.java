/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.measuring;

import java.io.File;
import static knapsack.Knapsack.hw3runTest;

/**
 *
 * @author etruska
 */
public class FileManager {

    public static void emptyFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                } else {
                    f.delete();
                }
            }
        }
    }
}
