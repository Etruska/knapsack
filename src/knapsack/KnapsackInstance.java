/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

import java.util.ArrayList;
import knapsack.Item;

/**
 *
 * @author etruska
 */
public class KnapsackInstance {

    private ArrayList<Item> items;

    private int maxWeight;

    private int id = 0;

    public KnapsackInstance() {
        items = new ArrayList<Item>();
    }

    public KnapsackInstance(KnapsackInstance source) {
        id = source.id;
        maxWeight = source.getMaxWeight();
        items = new ArrayList<Item>();
        for (Item i : source.getItems()) {
            items.add(new Item(i.getId(), i.getWeight(), i.getPrice(), i.isInSack()));
        }
    }

    public boolean isBetterThan(KnapsackInstance target) {

        return this.getCurrentPrice() > target.getCurrentPrice();
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public void randomizeKnapsack() {
        for (Item i : items) {
            i.setInSack(Math.random() < 0.5);
        }
    }

    public boolean isValid(){
        return getCurrentWeight() <= getMaxWeight();
    }
    
    public int getMaxWeight() {
        return maxWeight;
    }

    public void addItem(int id, int weight, int price) {
        items.add(new Item(id, weight, price));
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCurrentWeight() {
        int weight = 0;
        for (Item i : items) {
            if (i.isInSack()) {
                weight += i.getWeight();
            }
        }
        return weight;
    }

    public int getCurrentPrice() {
        int price = 0;
        for (Item i : items) {
            if (i.isInSack()) {
                price += i.getPrice();
            }
        }
        return price;
    }

    public int size() {
        return items.size();
    }

    public int getPriceSum() {
        int price = 0;
        for (Item i : items) {
            price += i.getPrice();
        }
        return price;
    }

    public int getMaxPrice() {
        int maxPrice = 0;
        for (Item i : items) {
            if (i.getPrice() > maxPrice) {
                maxPrice = i.getPrice();
            }
        }
        return maxPrice;
    }

    public double getAveragePrice(){
        return (double) getPriceSum() / this.items.size();
    }
    
    public void printSackState() {
        String result = "";
        for (Item i : items) {
            result += i.isInSack() ? "1" : "0";
        }
        System.out.println(result);
    }

    public void printItems() {
        String result = "";
        for (Item i : items) {
            result += "[" + i.getWeight() + "," + i.getPrice() + "] ";
        }
        System.out.println("KP " + id + ":" + result);

    }

}
