/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.solving;

import knapsack.solving.Solver;
import java.util.Collections;
import knapsack.IdComparator;
import knapsack.Item;
import knapsack.KnapsackInstance;
import knapsack.RatioComparator;

/**
 *
 * @author etruska
 */
public class SimpleRatioSolver extends Solver {

    public void solve() {

        Collections.sort(this.knapsack.getItems(), new RatioComparator());
        int currentWeight = 0;
        for (Item i : knapsack.getItems()) {
            if (currentWeight + i.getWeight() <= knapsack.getMaxWeight()) {
                i.setInSack(true);
                currentWeight += i.getWeight();
            } else {
                break;
            }

        }

        Collections.sort(this.knapsack.getItems(), new IdComparator());
        this.bestSolution = new KnapsackInstance(knapsack);

        if (printSolution) {
            printSolution();
        }

    }
}
