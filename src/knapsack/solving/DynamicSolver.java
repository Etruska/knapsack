/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack.solving;

import java.util.HashMap;
import knapsack.KnapsackInstance;
import knapsack.Item;

/**
 *
 * @author etruska
 */
public class DynamicSolver extends Solver {

    private HashMap<Integer, HashMap<Integer, Integer>> dArray;

    private int maxPrice = 0;

    public void solve() {

        this.maxPrice = getMaxPrice();
        this.dArray = new HashMap<Integer, HashMap<Integer, Integer>>();

        knap(0, 0, 0);
        setSolution();

        if (printSolution) {
            printSolution();
        }
    }

    private void knap(int price, int itemNum, int weight) {

        if (get(price, itemNum) != 0 && get(price, itemNum) <= weight) {
            return;
        }
        if (weight > knapsack.getMaxWeight()) {
            return;
        }

        put(price, itemNum, weight);

        if (itemNum >= knapsack.getItems().size()) {
            return;
        }

        Item nextItem = this.knapsack.getItems().get(itemNum);
        int nextWeight = weight + nextItem.getWeight();

        //sack without next item
        knap(price, itemNum + 1, weight);
        //sack with next item
        knap(price + nextItem.getPrice(), itemNum + 1, nextWeight);
    }
    
    private int get(int price, int itemNum) {
        if (dArray.get(itemNum) == null) {
            dArray.put(itemNum, new HashMap());
        }

        if (dArray.get(itemNum).get(price) == null) {
            dArray.get(itemNum).put(price, 0);
        }

        return dArray.get(itemNum).get(price);
    }

    private void put(int price, int itemNum, int weight) {
        if (dArray.get(itemNum) == null) {
            dArray.put(itemNum, new HashMap());
        }
        dArray.get(itemNum).put(price, weight);
    }

    private int getMaxPrice() {
        int sum = 0;
        for (Item i : this.knapsack.getItems()) {
            sum += i.getPrice();
        }
        return sum;
    }

    private void setSolution() {
        int itemCount = this.knapsack.getItems().size();
        for (int i = this.maxPrice; i >= 0; i--) {
            if (get(i, itemCount) != 0 || i == 0) { //found solution starting point
                if (get(i, itemCount) <= this.knapsack.getMaxWeight()) {
                    setItemInSack(itemCount, i);
                    break;
                }
            }
        }
    }

    private void setItemInSack(int itemNum, int price) {

        if (itemNum <= 0) {
            this.bestSolution = new KnapsackInstance(this.knapsack);
            return;
        }
        //items are numbered from 0 
        Item currItem = this.knapsack.getItems().get(itemNum - 1);
        //adjecent numbers equal -> item is not in sack
        if (get(price, itemNum - 1) == get(price, itemNum)) {
            currItem.setInSack(false);
            setItemInSack(itemNum - 1, price);

        } else {
            currItem.setInSack(true);
            setItemInSack(itemNum - 1, price - currItem.getPrice());
        }

    }

}
