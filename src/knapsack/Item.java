/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package knapsack;

/**
 *
 * @author etruska
 */
public class Item {
    
    private int weight;
    private int price;
    private int id;
    private boolean inSack = false;

    public Item(int id, int weight, int price) {
        this.id = id;
        this.weight = weight;
        this.price = price;
    }
    public Item(int id, int weight, int price, boolean inSack) {
        this.id = id;
        this.weight = weight;
        this.price = price;
        this.inSack = inSack;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public int getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }
    
    public double getRatio(){
        return getWeight() / Math.max(getPrice(), 0.00001);
    }

    public boolean isInSack() {
        return inSack;
    }

    public void invertInSack(){
        this.inSack = !this.inSack;
    }
    public void setInSack(boolean inSack) {
        this.inSack = inSack;
    }
    
}
