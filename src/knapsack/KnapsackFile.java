/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package knapsack;

import java.util.ArrayList;

/**
 *
 * @author etruska
 */
public class KnapsackFile {
 
    ArrayList<KnapsackInstance> instances;
    
    String filename;
    
    int knapsackSize;

    public int getKnapsackSize() {
        return knapsackSize;
    }

    public void setKnapsackSize(int knapsackSize) {
        this.knapsackSize = knapsackSize;
    }

    public KnapsackFile(ArrayList<KnapsackInstance> instances, String filename) {
        this.instances = instances;
        this.filename = filename;
    }

    public ArrayList<KnapsackInstance> getInstances() {
        return instances;
    }

    public String getFilename() {
        return filename;
    }
}
