/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package knapsack;

/**
 *
 * @author etruska
 */
public class Stopwatch {

    private long startTime;
    private long stopTime;
    private double divided = 1.0f;

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public void stop() {
        stopTime = System.currentTimeMillis();
        double elapsedTime = (stopTime - startTime) / divided;
        System.out.println("Elapsed time: " + elapsedTime + " ms");
    }

    public double getElapsedTime() {
        stopTime = System.currentTimeMillis();
        return (stopTime - startTime) / divided;
    }

    public double getElapsedTime(double divided) {
        this.divided = divided;
        return getElapsedTime();
    }

    public void stop(double divided) {
        this.divided = divided;
        stop();
    }

    public void setDivided(double divided) {
        this.divided = divided;
    }

}
